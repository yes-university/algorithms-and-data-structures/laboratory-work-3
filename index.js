const fs = require("fs");

const { arrayGen } = require("./array-gen");
const binarySearch = require("./binarySearch");
const directSearch = require("./directSearch");

const array = arrayGen(1000, -100, 100);
const sortedArray = [...array].sort((a, b) => a - b);

const randomIndex = Math.floor(Math.random() * sortedArray.length);
const target = sortedArray[randomIndex];

console.log("Array: ", JSON.stringify(array));
console.log("Sorted Array: ", JSON.stringify(sortedArray));
console.log("Random Index: ", randomIndex);
console.log("Target from random Index: ", target);
const result = binarySearch(sortedArray, target);
console.log(
  `Binary index Result: { [${result}]: ${sortedArray[result]} }`,
  sortedArray[result] === sortedArray[randomIndex] ? "PASS" : "FAIL"
);

console.log("--- SEARCH ---");
const searchText = fs.readFileSync("./search.txt", "utf8");

const splittedSearchText = searchText.split(" ");
const randomSearchPattern =
  splittedSearchText[Math.floor(Math.random() * splittedSearchText.length)];

console.log("Random Search Pattern: ", randomSearchPattern);

const directSearchResult = directSearch(searchText, randomSearchPattern);
console.log(
  "Direct Search Result: ",
  directSearchResult,
  searchText[directSearchResult] ===
    searchText[searchText.search(randomSearchPattern)]
    ? "PASS"
    : "FAIL"
);
