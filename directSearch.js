module.exports = function directSearch(text, pattern) {
  let i = 0;
  let j = 0;
  let k = 0;
  let result = -1;
  while (i < text.length) {
    if (text[i] === pattern[j]) {
      if (j === 0) {
        k = i;
      }
      j++;
      if (j === pattern.length) {
        result = k;
        break;
      }
    } else {
      j = 0;
    }
    i++;
  }
  return result;
};
